package com.task17_SpringCore_1.service;

public interface BeanValidator {
  boolean validate();
}
