package com.task17_SpringCore_1.model;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class BeanZ implements BeanFactoryPostProcessor, ApplicationContextAware {

  @Override
  public void postProcessBeanFactory(
      ConfigurableListableBeanFactory beanFactory) throws BeansException {
    BeanDefinition beanB = beanFactory.getBeanDefinition("beanB");
    beanB.setInitMethodName("init2");
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
    for (String beanDefinitionName : beanDefinitionNames) {
      System.out.println(beanDefinitionName);
    }
  }
}
