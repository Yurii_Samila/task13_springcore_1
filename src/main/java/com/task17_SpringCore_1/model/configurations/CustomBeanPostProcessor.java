package com.task17_SpringCore_1.model.configurations;

import com.task17_SpringCore_1.service.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CustomBeanPostProcessor implements BeanPostProcessor {

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    return bean;
  }
  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    if (bean instanceof BeanValidator) {
      ((BeanValidator) bean).validate();
    }
    return bean;
  }
}
