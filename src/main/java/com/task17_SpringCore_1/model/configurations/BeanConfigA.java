package com.task17_SpringCore_1.model.configurations;

import com.task17_SpringCore_1.model.BeanA;
import com.task17_SpringCore_1.model.BeanB;
import com.task17_SpringCore_1.model.BeanC;
import com.task17_SpringCore_1.model.BeanD;
import com.task17_SpringCore_1.model.BeanE;
import com.task17_SpringCore_1.model.BeanZ;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("my.properties")
@Import(BeanConfigB.class)
public class BeanConfigA {
  @Bean
  @DependsOn(value = {"beanB","beanC"})
  @Primary
  public BeanA getBeanA1(BeanB beanB, BeanC beanC){
    BeanA beanA = new BeanA();
    beanA.setName(beanB.getName().substring(0,4).concat("AAA"));
    beanA.setValue(beanC.getValue() - 200);
    return beanA;
  }
  @Bean("beanA2")
  @Qualifier("beanA2")
  @DependsOn(value = {"beanD","beanB"})
  public BeanA getBeanA2(BeanB getBeanB, BeanD getBeanD){
    BeanA beanA = new BeanA();
    beanA.setName(getBeanB.getName().substring(0,4).concat("AAA"));
    beanA.setValue(getBeanD.getValue() - 300);
    return beanA;
  }
  @Bean
  public BeanA getBeanA3(@Qualifier("beanC") BeanC cBean, @Qualifier("beanD")BeanD dBean){
    BeanA beanA = new BeanA();
    beanA.setName(dBean.getName().substring(0,4).concat("AAA"));
    beanA.setValue(cBean.getValue() - 200);
    return beanA;
  }
  @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = "beanB")
  public BeanC getBeanC(){
    BeanC beanC = new BeanC();
    return beanC;
  }
  @Bean("beanE1")
  @DependsOn(value = "getBeanA1")
  @Primary
  public BeanE getBeanE1(@Qualifier("getBeanA1") BeanA beanA){
    BeanE beanE = new BeanE(beanA);
    return beanE;
  }
  @Bean("beanE2")
  @DependsOn(value = "beanA2")
  public BeanE getBeanE2(@Qualifier("beanA2") BeanA beanA){
    BeanE beanE = new BeanE(beanA);
    return beanE;
  }
  @Bean("beanE3")
  @DependsOn(value = "getBeanA3")
  public BeanE getBeanE3(BeanA getBeanA3){
    BeanE beanE = new BeanE(getBeanA3);
    return beanE;
  }
  @Bean
  public BeanZ getBeanZ(){
    BeanZ beanZ = new BeanZ();
    return beanZ;
  }
}
