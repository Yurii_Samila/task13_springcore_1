package com.task17_SpringCore_1.model.configurations;

import com.task17_SpringCore_1.model.BeanB;
import com.task17_SpringCore_1.model.BeanD;
import com.task17_SpringCore_1.model.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("my.properties")
public class BeanConfigB {
  @Bean
  public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
  public BeanD getBeanD(){
    BeanD beanD = new BeanD();
    return beanD;
  }

  @Bean("beanF")
  @Lazy
  public BeanF getBeanF(){
    BeanF beanF = new BeanF();
    beanF.setName("BeanFFF");
    beanF.setValue(600);
    return beanF;
  }
  @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = "beanD")
  public BeanB getBeanB(){

    return new BeanB();
  }

}
