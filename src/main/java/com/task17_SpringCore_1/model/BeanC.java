package com.task17_SpringCore_1.model;

import com.task17_SpringCore_1.service.BeanValidator;
import org.springframework.beans.factory.annotation.Value;

public class BeanC implements BeanValidator {
  @Value("${beanC.name}")
  private String name;
  @Value("${beanC.value}")
  private int value;

  public void init(){
    System.out.println("Init method in " + this.name);
  }

  public void destroy(){
    System.out.println("Destroy method in " + this.name);
  }

  @Override
  public String toString() {
    return "BeanC{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int  value) {
    this.value = value;
  }

  @Override
  public boolean validate() {
    if (name == null){
      System.out.println("Name is null");
      return false;
    }else if (value < 0){
      System.out.println("Value is less than zero");
      return false;
    }
    System.out.println("Validation successful in " + name);
    return true;
  }
}
