package com.task17_SpringCore_1.model;

import com.task17_SpringCore_1.service.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class BeanF implements BeanValidator, BeanFactoryPostProcessor {
  private String name;
  private int value;

  @Override
  public String toString() {
    return "BeanF{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public boolean validate() {
    if (name == null){
      System.out.println("Name is null");
      return false;
    }else if (value < 0){
      System.out.println("Value is less than zero");
      return false;
    }
    System.out.println("Validation successful in " + name);
    return true;
  }

  @Override
  public void postProcessBeanFactory(
      ConfigurableListableBeanFactory beanFactory) throws BeansException {
    for (String name: beanFactory.getBeanDefinitionNames()){
      System.out.println(beanFactory.getBeanDefinition(name));
  }
}
}
