package com.task17_SpringCore_1.model;

import com.task17_SpringCore_1.service.BeanValidator;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanE implements BeanValidator, BeanPostProcessor {
  private String name;
  private int value;

  public BeanE(BeanA beanA) {
    this.name = beanA.getName();
    this.value = beanA.getValue();
  }

  @PostConstruct
  public void initMethod(){
    System.out.println("inside @PostConstruct");
  }
  @PreDestroy
  public void destroyMethod(){
    System.out.println("inside @PreDestroy");
  }

  @Override
  public String toString() {
    return "BeanE{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public boolean validate() {
    if (name == null){
      System.out.println("Name is null");
      return false;
    }else if (value < 0){
      System.out.println("Value is less than zero");
      return false;
    }
    System.out.println("Validation successful in " + name);
    return true;
  }
}
