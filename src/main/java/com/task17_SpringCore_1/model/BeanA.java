package com.task17_SpringCore_1.model;

import com.task17_SpringCore_1.service.BeanValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
  private String name;
  private int value;

  @Override
  public String toString() {
    return "BeanA{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public boolean validate() {
  if (name == null){
    System.out.println("Name is null");
    return false;
  }else if (value < 0){
    System.out.println("Value is less than zero");
    return false;
  }
    System.out.println("Validation successful in " + name);
  return true;
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("Destroying " + this.name);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    System.out.println("AfterPropertiesSet in " + this.name);
  }
}
