package com.task17_SpringCore_1.controller;

import com.task17_SpringCore_1.model.BeanA;
import com.task17_SpringCore_1.model.BeanB;
import com.task17_SpringCore_1.model.BeanC;
import com.task17_SpringCore_1.model.configurations.BeanConfigA;
import com.task17_SpringCore_1.model.BeanD;
import com.task17_SpringCore_1.model.BeanE;
import com.task17_SpringCore_1.model.BeanF;
import com.task17_SpringCore_1.model.BeanZ;
import com.task17_SpringCore_1.model.configurations.CustomBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

  public static void main(String[] args) {
    CustomBeanPostProcessor customBeanPostProcessor = new CustomBeanPostProcessor();
    ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigA.class);
    BeanA beanA = context.getBean(BeanA.class);
    customBeanPostProcessor.postProcessAfterInitialization(beanA,beanA.getName());
    context.getBean(BeanB.class);
    context.getBean(BeanC.class);
    context.getBean(BeanD.class);
    context.getBean(BeanE.class);
    context.getBean(BeanF.class);
    context.getBean(BeanZ.class);
    ((AnnotationConfigApplicationContext) context).close();
  }
}
